<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_Base
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

namespace ETWS\Base\Logger\Handler;

use Magento\Framework\Logger\Handler\Base as MagentoBase;

/**
 * Class Base
 *
 * @package ETWS\Base\Logger\Handler
 * @author Alexey Yerofeyev <a.yerofeyev@etwebsolutions.com>
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 */
class Base extends MagentoBase
{
    /**
     * @var string
     */
    protected $fileName = '/var/log/etws.log';


    /**
     * Set log file name
     *
     * @param null|string $filename
     */
    public function setFilename($filename = null)
    {
        if ($filename) {
            $this->fileName = $filename;
            $this->url = BP . '/var/log/' . $this->fileName;
        }
    }

    /**
     * @inheritdoc
     */
    public function isHandling(array $record)
    {
        return true;
    }
}
