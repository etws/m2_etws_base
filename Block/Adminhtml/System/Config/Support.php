<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_Base
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

namespace ETWS\Base\Block\Adminhtml\System\Config;

use Magento\Framework\View\Element\Template;
use Magento\Backend\Model\Session;
use Magento\Backend\Model\Locale\Manager;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\ObjectManagerInterface;
use ETWS\Base\Helper\AbstractHelper as Helper;
use Magento\Framework\App\ProductMetadataInterface;

/**
 * Class Support
 * @method string getName() Retrieve extension name.
 *
 * @package ETWS\Base\Block\Adminhtml\System\Config
 * @author Alexey Yerofeyev <a.yerofeyev@etwebsolutions.com>
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 */
class Support extends Template
{
    /**
     * @var string
     */
    protected $_template = 'support/info.phtml';

    /**
     * @var Session
     */
    protected $_session;

    /**
     * @var Manager
     */
    protected $_localeManager;

    /**
     * @var ModuleListInterface
     */
    protected $_moduleList;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var Helper
     */
    protected $_helper;

    /**
     * @var array
     */
    protected $_fieldConfig;

    /**
     * @var array
     */
    protected $_extensionInfo;

    /**
     * @var array
     */
    protected $_extensionConfigData;

    /**
     * @var ProductMetadataInterface
     */
    protected $_productMetadata;

    /**
     * Support constructor.
     *
     * @param Template\Context $context
     * @param Session $session
     * @param Manager $localeManager
     * @param ModuleListInterface $moduleList
     * @param ObjectManagerInterface $objectManager
     * @param ProductMetadataInterface $productMetadata
     */
    public function __construct(
        Template\Context $context,
        Session $session,
        Manager $localeManager,
        ModuleListInterface $moduleList,
        ObjectManagerInterface $objectManager,
        ProductMetadataInterface $productMetadata
    )
    {
        parent::__construct($context, []);
        $this->_session = $session;
        $this->_localeManager = $localeManager;
        $this->_moduleList = $moduleList;
        $this->_objectManager = $objectManager;
        $this->_productMetadata = $productMetadata;
    }

    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();
        $this->_getExtensionInfoConfig();
        return $this;
    }

    /**
     * Retrieve config array of field.
     *
     * @return array
     */
    protected function _getFieldConfig()
    {
        if (!$this->_fieldConfig) {
            $this->_fieldConfig = (array)$this->getData('field_config');
        }
        return $this->_fieldConfig;
    }

    /**
     * @return array
     */
    protected function _getExtensionInfo()
    {
        if (!is_array($this->_extensionInfo) || (is_array($this->_extensionInfo) && count($this->_extensionInfo) < 2)) {
            $extensionName = $this->getInfoExtensionName();
            $extensionInfo = $this->_moduleList->getOne($extensionName);
            if (!is_array($extensionInfo)) {
                throw new \InvalidArgumentException("Unknown Extension: \"$extensionName\"");
            }
            $this->_extensionInfo = $extensionInfo;
        }
        return $this->_extensionInfo;
    }

    /**
     * Before rendering html
     *
     * @return Helper
     */
    protected function _getHelper()
    {
        if (!$this->_helper) {
            $nameSpace = str_replace('ETWS_', '', $this->getInfoExtensionName());
            $class = $class = '\ETWS\\' . $nameSpace . '\Helper\Data';
            $helper = $this->_objectManager->get($class);
            $this->_helper = $helper;
        }
        return $this->_helper;
    }

    /**
     * Load extension information from config.xml
     *
     * @return array
     */
    protected function _getExtensionInfoConfig()
    {
        if ($this->_extensionConfigData) {
            return $this->_extensionConfigData;
        }

        $config = (array)$this->_getHelper()->getExtensionConfig('info');

        $locale = $this->_localeManager->getUserInterfaceLocale();
        if (!$locale) {
            $locale = $this->_session->getData('locale');
        }

        $info = [];
        foreach ($config as $key => $configValue) {
            $value = '';

            if (is_array($configValue)) {
                if (!empty($configValue[$locale])) {
                    $value = $configValue[$locale];
                } elseif (!empty($configValue['en_US'])) {
                    $value = $configValue['en_US'];
                }
            } else {
                $value = $configValue;
            }

            $info[$key] = $value;
        }
        $this->addData($info);
        $this->_extensionConfigData = $info;
        return $this->_extensionConfigData;
    }

    /**
     * Retrieve extension name from system.xml
     *
     * @return string
     */
    public function getInfoExtensionName()
    {
        if (!isset($this->_extensionInfo['name'])) {
            $fieldConfig = $this->_getFieldConfig();
            if (!isset($fieldConfig['module'])) {
                throw new \InvalidArgumentException('Extension Name isn\'t defined.');
            }
            $this->_extensionInfo['name'] = $fieldConfig['module'];
        }
        return $this->_extensionInfo['name'];
    }

    /**
     * Get extension name
     *
     * @return string
     */
    public function getExtensionName()
    {
        $config = $this->_getExtensionInfoConfig();
        if(!isset($config['name'])) {
            return $this->getInfoExtensionName();
        }
        return $config['name'];
    }

    /**
     * Get extension version
     *
     * @return array
     */
    public function getVersion(){
        $extensionInfo = $this->_getExtensionInfo();
        return $extensionInfo['setup_version'];
    }

    /**
     * Build and return link to ETWS logo
     *
     * @return string
     */
    public function getLogoUrl()
    {
        $magentoVersion = $this->_productMetadata->getVersion();
        $magentoPlatform = 'ce';
        $logo = 'https://shop.etwebsolutions.com/logotypes-m2/' .
            $magentoPlatform . '/' .
            $magentoVersion . '/' .
            $this->getExtensionName() . '/' .
            $this->getVersion() . '/' .
            'logo.png';
        return $logo;
    }
}
