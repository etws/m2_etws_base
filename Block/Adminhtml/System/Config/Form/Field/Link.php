<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_Base
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

namespace ETWS\Base\Block\Adminhtml\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Config\Block\System\Config\Form\Field;

/**
 * Class Link
 * @package ETWS\CurrencyRateUpdater\Block\Adminhtml\Config\Form\Field
 */

/**
 * Class Link
 *
 * @package ETWS\Base\Block\Adminhtml\System\Config\Form\Field
 * @author Alexey Yerofeyev <a.yerofeyev@etwebsolutions.com>
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 */
class Link extends Field
{
    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $config = $element->getData('field_config');

        $params = [];
        if (!empty($config['paramName']) && isset($config['paramValue'])) {
            $params[$config['paramName']] = $config['paramValue'];
        }

        $url = $this->_urlBuilder->getUrl(
            $config['url'],
            $params
        );

        $textToTranslate = $config['text'];
        $html = '<a href="' . $url . '">' . __($textToTranslate) . '</a>';

        if (isset($config['comment'])) {
            $textToTranslate = $config['comment'];
            $html .= '<p class="note"><span>' . __($textToTranslate) . '</span></p>';
        }

        $textToTranslate = $config['label'];
        $html = '<tr><td class="label">' . __($textToTranslate) . '</td><td><span>' . $html . '</span></td></tr>';

        return $html;
    }
}
