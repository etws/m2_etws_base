<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_Base
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

namespace ETWS\Base\Block\Adminhtml\System\Config\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Config\Block\System\Config\Form\Field\Heading as MagentoCoreHeading;

/**
 * Class Heading
 * @package ETWS\Base\Block\Adminhtml\System\Config\Form\Field
 */
class Heading extends MagentoCoreHeading
{
    /**
     * @inheritdoc
     *
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $config = $element->getData();

        $textToTranslate = $config['label'];

        return sprintf(
            '<tr class="system-fieldset-sub-head" id="row_%s"><td colspan="5"><h4 id="%s">'
            . '<span class="etws-system-fieldset-sub-head">%s</span></h4></td></tr>',
            $element->getHtmlId(),
            $element->getHtmlId(),
            __($textToTranslate)
        );
    }
}
