<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_Base
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

namespace ETWS\Base\Block\Adminhtml\System\Config\Form\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class Support
 *
 * @package ETWS\Base\Block\Adminhtml\System\Config\Form\Field
 * @author Alexey Yerofeyev <a.yerofeyev@etwebsolutions.com>
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 */
class Support extends Field
{
    /**
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $field_config = $element->getData('field_config');
        /** @var \ETWS\Base\Block\Adminhtml\System\Config\Support $block */
        $block = $this->_layout->createBlock('ETWS\Base\Block\Adminhtml\System\Config\Support');
        $block->setData('field_config', $field_config);

        return $block->toHtml();
    }
}
