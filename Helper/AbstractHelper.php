<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_Base
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

namespace ETWS\Base\Helper;

use ETWS\Base\Logger\Logger;
use Magento\Framework\App\Helper\AbstractHelper as MagentoAbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

/**
 * Class AbstractHelper
 *
 * @package ETWS\Base\Helper
 * @author Alexey Yerofeyev <a.yerofeyev@etwebsolutions.com>
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 */
abstract class AbstractHelper extends MagentoAbstractHelper
{
    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @param Context $context
     * @param Logger $logger
     */
    public function __construct(
        Context $context,
        Logger $logger
    )
    {
        parent::__construct($context);

        $logFileName = $this->getLogFileName();
        /** @var \ETWS\Base\Logger\Handler\Base $handler */
        foreach ($logger->getHandlers() as $handler) {
            $handler->setFilename($logFileName);
        }
        $this->_logger = $logger;
    }

    /**
     * Retrieve extension part of config path.
     *
     * @return string
     */
    abstract protected function _getConfigSectionId();

    /**
     * Retrieve config value by path.
     *
     * @param string $path The path through the tree of configuration values, e.g., 'general/store_information/name'
     * @param string $scopeType The scope to use to determine config value, e.g., 'store' or 'default'
     * @param null|string $scopeCode
     * @return mixed
     */
    public function getConfig($path, $scopeCode = null, $scopeType = null)
    {
        if (null === $scopeType) {
            $scopeType = ScopeInterface::SCOPE_STORE;
        }
        return $this->scopeConfig->getValue($path, $scopeType, $scopeCode);
    }

    /**
     * Retrieve config value from extension settings.
     *
     * @param string $path The path through the tree of configuration values, e.g., 'general/store_information/name'
     * @param string $scopeType The scope to use to determine config value, e.g., 'store' or 'default'
     * @param null|string $scopeCode
     * @return mixed
     */
    public function getExtensionConfig($path, $scopeCode = null, $scopeType = null)
    {
        return $this->getConfig($this->_getConfigSectionId() . '/' . $path, $scopeType, $scopeCode);
    }

    /**
     * Writes information to log file
     *
     * @param $message - string or array of strings
     * @return bool
     */
    public function log($message)
    {
        if ($this->isLogEnabled()) {
            if (is_array($message)) {
                $forLog = array();
                foreach ($message as $answerKey => $answerValue) {
                    $forLog[] = $answerKey . ": " . $answerValue;
                }
                $forLog[] = '***************************';
                $message = implode("\r\n", $forLog);
            }
            $this->_logger->log(\Monolog\Logger::DEBUG, $message);
        }
        return true;
    }

    /**
     * Is log writing enabled or not?
     *
     * @return bool
     */
    public function isLogEnabled()
    {
        return (bool)$this->getExtensionConfig('general/log_enabled');
    }

    /**
     * Returns log file name
     *
     * @return string
     */
    public function getLogFileName()
    {
        $logFile = $this->getExtensionConfig('general/log_file');
        if ($logFile == '') {
            $logFile = $this->_getConfigSectionId() . '.log';
        }
        return $this->_getConfigSectionId() . '/' . date("Y.m.d-") . $logFile;
    }
}