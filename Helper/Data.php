<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category  ETWS
 * @package   ETWS_Base
 * @copyright Copyright (c) 2017 ET Web Solutions (http://etwebsolutions.com)
 * @contacts  support@etwebsolutions.com
 * @license  https://shop.etwebsolutions.com/etws-license-free-v1/   ETWS Free License (EFL1)
 */

namespace ETWS\Base\Helper;

/**
 * Class Data
 *
 * @package ETWS\Base\Helper
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 */
class Data extends AbstractHelper
{
    /**
     * @inheritdoc
     */
    protected function _getConfigSectionId()
    {
        return 'etws_base';
    }

}